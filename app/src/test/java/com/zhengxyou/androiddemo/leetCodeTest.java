package com.zhengxyou.androiddemo;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * 力扣leetCode算法题
 * <p>
 * PriorityQueue优先级队列
 */
public class leetCodeTest {
    /**
     * <a href="https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/">无重复字符的最长子串</a>
     */
    @Test
    public void lengthOfLongestSubstring() {
        String test = " ";

        StringBuilder builder = new StringBuilder();
        int max = 0;
        String maxStr = "";
        for (int i = 0; i < test.length(); i++) {
            String sub = test.substring(i, i + 1);
            int index = builder.indexOf(sub);
            if (index != -1) {
                //存在
                if (max < builder.length()) {
                    max = builder.length();
                    maxStr = builder.toString();
                }
                builder = new StringBuilder(builder.substring(index + 1));
                builder.append(sub);
            } else {
                builder.append(sub);
            }
        }
        if (max < builder.length()) {
            max = builder.length();
        }
        System.out.println("Max length:" + max + ",str:" + maxStr);
    }


    /**
     * 两数之和
     * <p>
     * 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
     */
    @Test
    public void sumOfTwoNumbers() {
        int[] nums = {1, 4, 6, 3, 7, 9, 4};
        int target = 11;
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    System.out.println(i + "," + j);
//                    break;
                }
            }
        }
        System.out.println("============");
        Map<Integer, Integer> integerMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer integer = integerMap.get(target - nums[i]);
            if (integer != null) {
                System.out.println(i + "," + integer);
            }
            integerMap.put(nums[i], i);
        }
    }

    /**
     * <a href="https://leetcode-cn.com/problems/add-two-numbers/submissions/">两数相加</a>
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    @Test
    public void addTwoNumbers() {
        ListNode l1, l2;
        l1 = new ListNode(9);


        l2 = new ListNode(1);
        ListNode l21 = new ListNode(9);
        ListNode l22 = new ListNode(9);
        ListNode l23 = new ListNode(9);
        ListNode l24 = new ListNode(9);
        ListNode l25 = new ListNode(9);
        ListNode l26 = new ListNode(9);
        ListNode l27 = new ListNode(9);
        ListNode l28 = new ListNode(9);
        ListNode l29 = new ListNode(9);
        l2.next = l21;
        l21.next = l22;
        l22.next = l23;
        l23.next = l24;
        l24.next = l25;
        l25.next = l26;
        l26.next = l27;
        l27.next = l28;
        l28.next = l29;

//        int sum = s(l1, 0) + s(l2, 0);
//        System.out.println(s(l1, 0));
//        System.out.println(s(l2, 0));
//        System.out.println(sum);
//        ListNode node = getNode(sum);
        s(getNode(l1, l2));
    }

    private ListNode getNode(ListNode left, ListNode right) {
        int lv = 0;
        int rv = 0;
        if (left != null) {
            lv = left.val;
        }
        if (right != null) {
            rv = right.val;
        }

        int sum = lv + rv;
        ListNode sumNode;
        //大于10进1位
        if (sum >= 10) {
            sumNode = new ListNode(sum % 10);

            if (left != null && left.next != null) {
                left.next.val += 1;
            } else if (right != null && right.next != null) {
                right.next.val += 1;
            } else {
                sumNode.next = new ListNode(1);
            }

        } else {
            sumNode = new ListNode(sum);
        }

        ListNode nextL = null;
        ListNode nextR = null;

        if (left != null && left.next != null) {
            nextL = left.next;
        }
        if (right != null && right.next != null) {
            nextR = right.next;
        }

        if (nextL != null || nextR != null) {
            sumNode.next = getNode(nextL, nextR);
        }

        return sumNode;
    }

    private ListNode getNode(int sum) {
        ListNode node = new ListNode(sum % 10);
        if (sum > 10) {
            node.next = getNode(sum / 10);
        }
        return node;
    }

    private void s(ListNode node) {
        System.out.print(node.val + "->");
        if (node.next != null) {
            s(node.next);
        }
    }

    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}

package com.zhengxyou.androiddemo;

import org.junit.Test;

/**
 * <pre>
 *     author : Administrator
 *     e-mail : xxx@xx
 *     time   : 2019/07/15
 *     desc   : 二叉树测试
 *     version: 1.0
 * </pre>
 * 通过先序和中序或者中序和后序我们可以还原出原始的二叉树，但是通过先序和后续是无法还原出原始的二叉树的
 * <p>
 * 也就是说：通过中序和先序或者中序和后序我们就可以确定一颗二叉树了！
 */
public class BinaryTreeTest {
    /**
     * 动态创建二叉树
     */
    @Test
    public void dynamicCreationTreeNode() {
        int[] arrays = {2, 3, 1, 4, 5};
        TreeRoot treeRoot = new TreeRoot();
        for (int i : arrays) {
            createTree(treeRoot, i);
        }

        preTraverseBTree(treeRoot.getTreeRoot());
        System.out.println("=======");
        ineTraverseBTree(treeRoot.getTreeRoot());
        System.out.println("二叉树的深度");
        System.out.println(getHeight(treeRoot.getTreeRoot()));
        System.out.println("二叉树的最大值");
        System.out.println(getMax(treeRoot.getTreeRoot()));

    }

    private int getMax(TreeNode treeNode) {
        if (treeNode == null) {
            return -1;
        }
        int left = getMax(treeNode.lfetNode);

        int right = getMax(treeNode.rightNode);

        int root = treeNode.value;

        int max = left;
        if (right > max) {
            max = right;
        }
        if (root > max) {
            max = root;
        }
        return max;
    }

    private int getHeight(TreeNode node) {
        if (node == null) {
            return 0;
        }
        //左边
        int left = getHeight(node.lfetNode);

        //右边
        int right = getHeight(node.rightNode);

        int max = left;
        if (right > max) {
            max = right;
        }
        return max + 1;
    }

    private void createTree(TreeRoot root, int value) {
        if (root.getTreeRoot() == null) {
            root.setTreeRoot(new TreeNode(value));
        } else {
            TreeNode treeRoot = root.getTreeRoot();
            while (treeRoot != null) {
                //当前值大于根值，往右边走
                if (value >= treeRoot.value) {
                    //右边没有树根，那就直接插入
                    if (treeRoot.rightNode == null) {
                        treeRoot.rightNode = new TreeNode(value);
                        return;
                    } else {
                        //如果右边有树根，到右边的树根去
                        treeRoot = treeRoot.rightNode;
                    }
                } else {
                    //左没有树根，那就直接插入
                    if (treeRoot.lfetNode == null) {
                        treeRoot.lfetNode = new TreeNode(value);
                        return;
                    } else {
                        //如果左有树根，到左边的树根去
                        treeRoot = treeRoot.lfetNode;
                    }
                }
            }
        }

    }

    public class TreeRoot {
        private TreeNode treeRoot;

        public TreeNode getTreeRoot() {
            return treeRoot;
        }

        public void setTreeRoot(TreeNode treeRoot) {
            this.treeRoot = treeRoot;
        }
    }

    @Test
    public void testTreeNode() {
        //根节点-->10
        TreeNode n1 = new TreeNode(10);
        //左孩子-->9
        TreeNode n2 = new TreeNode(9);
        //右孩子-->20
        TreeNode n3 = new TreeNode(20);
        //20的左孩子-->15
        TreeNode n4 = new TreeNode(15);
        //20的右孩子-->35
        TreeNode n5 = new TreeNode(35);

        n1.lfetNode = n2;
        n1.rightNode = n3;

        n3.lfetNode = n4;
        n3.rightNode = n5;

        preTraverseBTree(n1);
        System.out.println("=======");
        ineTraverseBTree(n1);
    }

    /**
     * 中序遍历
     *
     * @param treeNode 根节点
     */
    public void ineTraverseBTree(TreeNode treeNode) {
        if (treeNode != null) {

            //访问左节点
            ineTraverseBTree(treeNode.lfetNode);
            //根节点
            System.out.println(treeNode.value);
            //访问右节点
            ineTraverseBTree(treeNode.rightNode);
        }
    }

    /**
     * 先序遍历
     *
     * @param treeNode 根节点
     */
    public void preTraverseBTree(TreeNode treeNode) {
        if (treeNode != null) {
            //根节点
            System.out.println(treeNode.value);
            //访问左节点
            preTraverseBTree(treeNode.lfetNode);
            //访问右节点
            preTraverseBTree(treeNode.rightNode);
        }
    }

    class TreeNode {
        int value;
        TreeNode lfetNode;
        TreeNode rightNode;

        public TreeNode(int value) {
            this.value = value;
        }
    }
}

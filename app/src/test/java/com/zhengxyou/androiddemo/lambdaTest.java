package com.zhengxyou.androiddemo;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
//https://www.cnblogs.com/andywithu/p/7404101.html
//https://segmentfault.com/a/1190000009186509
public class lambdaTest {
    @Test
    public void testList() {
        String[] atp = {"Rafael Nadal", "Novak Djokovic",
                "Stanislas Wawrinka",
                "David Ferrer", "Roger Federer",
                "Andy Murray", "Tomas Berdych",
                "Juan Martin Del Potro"};
        List<String> players = Arrays.asList(atp);
        players.forEach((value) -> System.out.println(value + ";"));

        players.forEach(System.out::println);

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        System.out.print("输出所有数字：");
        evaluate(list, n -> true);
        System.out.print("不输出：");
        evaluate(list, (n) -> false);
        System.out.print("输出偶数：");
        evaluate(list, n -> n % 2 == 0);
        System.out.print("输出奇数：");
        evaluate(list, n -> n % 2 == 1);
        System.out.print("输出大于 5 的数字：");
        evaluate(list, n -> n > 5);

        List<Integer> list1 = Arrays.asList(1, 4, 5, 6, 7, 2, 3);
        for (Integer n : list1) {
            System.out.println(n);
        }
        list1.forEach(n -> System.out.println(n));
        list1.forEach(System.out::println);

        for (Integer n : list1) {
            int x = n * n;
            System.out.println(x);
        }
        list1.stream().map(x -> x * x).forEach(System.out::println);

        Integer sum = list1.stream().map(x -> x * x).reduce((x, y) -> x + y).get();
        System.out.println(sum);

        List<Integer> collect = list1.stream().filter(n -> n % 2 == 0)
                .sorted()
                .map(n -> n * n)
                .limit(2)
                .collect(Collectors.toList());
        collect.forEach(System.out::println);
    }

    private void evaluate(List<Integer> list, Predicate<Integer> predicate) {
        for (Integer n : list) {
            if (predicate.test(n)) {
                System.out.print(n + " ");
            }
        }
        System.out.println();
    }

    void tmpExample() {
        Runnable r = () -> System.out.println("hello word");
        new Thread(() -> System.out.println("hello word")).start();
        WorkerInterface workerInterface = () -> System.out.println("通过Lambda表达式调用");
    }

    @FunctionalInterface
    public interface WorkerInterface {
//        public void doSomeWork();

        public void doWork();
    }
}

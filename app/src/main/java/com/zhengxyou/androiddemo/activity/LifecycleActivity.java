package com.zhengxyou.androiddemo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.zhengxyou.androiddemo.R;
import com.zhengxyou.androiddemo.activity.fragments.AFragment;
import com.zhengxyou.androiddemo.activity.fragments.BFragment;

/**
 * onPause,onDestroy都可以做资源释放，看类型,<br>
 * onPause，onStop都可以存储，但是建议在onStop中做处理，onPause时间短，处理大点的数据会有影响<br>
 * 要想添加没有 UI 的片段，请使用 add(Fragment, String) 从 Activity 添加片段（为片段提供一个唯一的字符串“标记”，而不是视图 ID）。
 * 这会添加片段，但由于它并不与 Activity 布局中的视图关联，因此不会收到对 onCreateView() 的调用。因此，您不需要实现该方法。
 */
public class LifecycleActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = LifecycleActivity.class.getSimpleName();
    private FrameLayout mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lifecycle);
        Log.e(TAG, "===onCreate===");

        initView();
        initData();
    }

    private void initData() {
    }

    private void initView() {
        findViewById(R.id.btn_activity_b).setOnClickListener(v -> BActivity.start(LifecycleActivity.this));
        findViewById(R.id.btn_activity_addFragment).setOnClickListener(this);
        findViewById(R.id.btn_activity_changeFragment).setOnClickListener(this);
        findViewById(R.id.btn_activity_changeFragmentText).setOnClickListener(this);
        mContent = findViewById(R.id.fl_activity_content);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "===onStart===");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.e(TAG, "===onRestoreInstanceState===");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "===onResume===");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "===onPause===");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "===onStop===");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.e(TAG, "===onSaveInstanceState===");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "===onDestroy===");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "===onRestart===");
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, LifecycleActivity.class);
        context.startActivity(starter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_activity_addFragment:
                getSupportFragmentManager().beginTransaction().add(R.id.fl_activity_content, AFragment.newInstance(), "AFragment").commit();
                break;
            case R.id.btn_activity_changeFragment:
                getSupportFragmentManager().beginTransaction().replace(R.id.fl_activity_content, BFragment.newInstance(), "BFragment")
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.btn_activity_changeFragmentText:
                AFragment aFragment = (AFragment) getSupportFragmentManager().findFragmentById(R.id.fm_a);
                break;
        }
    }
}

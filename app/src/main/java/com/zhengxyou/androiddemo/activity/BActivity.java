package com.zhengxyou.androiddemo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.zhengxyou.androiddemo.R;

public class BActivity extends AppCompatActivity {
    private static final String TAG = BActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        Log.e(TAG, "===onCreate===");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.e(TAG, "===onRestoreInstanceState===");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "===onStart===");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "===onResume===");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "===onPause===");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.e(TAG, "===onSaveInstanceState===");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "===onStop===");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "===onRestart===");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "===onDestroy===");
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, BActivity.class);
        context.startActivity(starter);
    }
}

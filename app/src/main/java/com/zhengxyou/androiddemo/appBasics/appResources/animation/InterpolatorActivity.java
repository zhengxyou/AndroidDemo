package com.zhengxyou.androiddemo.appBasics.appResources.animation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.zhengxyou.androiddemo.R;

/**
 * <a href="https://www.jianshu.com/p/88740cba25e6">参考</a>
 * <h3>自定义插值器</h3>
 * <p>
 * 有2种方式自定义：
 * <ol>
 * <li>代码</li>
 * <li>xml文件:
 * <p>语法：<br>
 * <pre>
 * <InterpolatorName xmlns:android="http://schemas.android.com/apk/res/android"
 * android:attribute_name="value"
 * />
 * </pre>
 * 如果不应用任何属性，那么插值器的功能与原先的功能完全相同
 * </p>
 * </li>
 * </ol>
 * </p>
 * <p>自定义插值器的话需要实现 {@link Interpolator}接口</p>
 * <div>
 * 在/res/anim文件夹下，我们可以自定义xml插值器，根标签写我们需要的插值器，然后系统提供一些可以设置的属性：
 *
 * <ol>
 * <li><accelerateDecelerateInterpolator> 没有属性</li>
 * <li><accelerateInterpolator> android:factor 浮点值，加速速率，默认为1</li>
 * <li><anticipateInterpolator> android:tension 浮点值，起始点后退的张力、拉力数，默认为2</li>
 * <li><anticipateOvershootInterpolator> android:tension 同上 android:extraTension 浮点值，拉力的倍数，默认为1.5（2 * 1.5）</li>
 * <li><bounceInterpolator> 没有属性</li>
 * <li><cycleInterpolator>  android:cycles 整数值，循环的个数，默认为1</li>
 * <li><decelerateInterpolator> android:factor 浮点值，减速的速率，默认为1</li>
 * <li><linearInterpolator> 没有属性</li>
 * <li><overshootInterpolator> 浮点值，超出终点后的张力、拉力，默认为2</li>
 * </ol>
 *
 * </div>
 */
public class InterpolatorActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView mTarget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interpolator);

        mTarget = findViewById(R.id.iv_resources_target);

        findViewById(R.id.btn_resources_accelerate_decelerate).setOnClickListener(this);
        findViewById(R.id.btn_resources_accelerate).setOnClickListener(this);
        findViewById(R.id.btn_resources_anticipate).setOnClickListener(this);
        findViewById(R.id.btn_resources_anticipateOvershoot).setOnClickListener(this);
        findViewById(R.id.btn_resources_bounce).setOnClickListener(this);
        findViewById(R.id.btn_resources_cycle).setOnClickListener(this);
        findViewById(R.id.btn_resources_decelerate).setOnClickListener(this);
        findViewById(R.id.btn_resources_linear).setOnClickListener(this);
        findViewById(R.id.btn_resources_overshoot).setOnClickListener(this);
        findViewById(R.id.btn_resources_customizeByCode).setOnClickListener(this);
        findViewById(R.id.btn_resources_customizeByXml).setOnClickListener(this);
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, InterpolatorActivity.class);
        context.startActivity(starter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_resources_accelerate_decelerate:
                AccelerateDecelerateInterpolator accelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();
                displayTranslate(accelerateDecelerateInterpolator);
                break;
            case R.id.btn_resources_accelerate:
                AccelerateInterpolator accelerateInterpolator = new AccelerateInterpolator();
                displayTranslate(accelerateInterpolator);
                break;
            case R.id.btn_resources_anticipate:
                AnticipateInterpolator anticipateInterpolator = new AnticipateInterpolator();
                displayTranslate(anticipateInterpolator);
                break;
            case R.id.btn_resources_anticipateOvershoot:
                AnticipateOvershootInterpolator anticipateOvershootInterpolator = new AnticipateOvershootInterpolator();
                displayTranslate(anticipateOvershootInterpolator);
                break;
            case R.id.btn_resources_bounce:
                BounceInterpolator bounceInterpolator = new BounceInterpolator();
                displayTranslate(bounceInterpolator);
                break;
            case R.id.btn_resources_cycle:
                //循环次数
                CycleInterpolator cycleInterpolator = new CycleInterpolator(3);
                displayTranslate(cycleInterpolator);
                break;
            case R.id.btn_resources_decelerate:
                DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator();
                displayTranslate(decelerateInterpolator);
                break;
            case R.id.btn_resources_linear:
                LinearInterpolator linearInterpolator = new LinearInterpolator();
                displayTranslate(linearInterpolator);
                break;
            case R.id.btn_resources_overshoot:
                OvershootInterpolator overshootInterpolator = new OvershootInterpolator();
                displayTranslate(overshootInterpolator);
                break;
            case R.id.btn_resources_customizeByCode://自定义的话需要实现Interpolator接口
                CustomInterpolate customInterpolate = new CustomInterpolate();
                displayTranslate(customInterpolate);
                break;
            case R.id.btn_resources_customizeByXml://自定义
                Interpolator interpolator = AnimationUtils.loadInterpolator(InterpolatorActivity.this, R.anim.custom_overshoot_interpolate);
                displayTranslate(interpolator);
                break;
        }
    }

    private void displayTranslate(Interpolator interpolator) {
        TranslateAnimation translateAnimation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_PARENT, 0.7f
                ,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_PARENT, 0
        );

        translateAnimation.setDuration(500);
        translateAnimation.setRepeatCount(0);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(interpolator);
        mTarget.startAnimation(translateAnimation);

    }
}

package com.zhengxyou.androiddemo.appBasics.appResources.drawable;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ScaleDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.zhengxyou.androiddemo.R;

/**
 * <a href="https://developer.android.google.cn/guide/topics/resources/drawable-resource">可绘制对象资源</a>
 * 所有的xml资源文件都有对应的类
 * <a href="https://developer.android.google.cn/reference/android/graphics/drawable/ColorDrawable">参考地址</a>
 * <ol>
 * <li>位图图形文件(.png,.jpg或.gif)--BitmapDrawable</li>
 * <li>九宫格文件，具有可拉伸区域的PNG文件，允许根据内容调整图像大小 <code>.9.png</code></li>
 * <li>图层列表，按阵列顺序绘制，索引最大的元素绘制在顶部。--LayerDrawable</li>
 * <li>状态列表--StateListDrawable</li>
 * <li>级别列表，LevelListDrawable</li>
 * <li>转换可绘制对象，用于定义可在两种可绘制对象资源之间交错淡出的可绘制对象。TransitionDrawable</li>
 * <li>插入可绘制对象，此 XML 文件用于定义以指定距离插入其他可绘制对象的可绘制对象。当视图需要小于视图实际边界的背景可绘制对象时，此类可绘制对象很有用</li>
 * <li>裁剪可绘制对象:对其他可绘制对象进行裁剪--ClipDrawable</li>
 * <li>缩放可绘制对象:ScaleDrawable</li>
 * <li>形状可绘制对象：ShapeDrawable,用于定义几何形状</li>
 * </ol>
 *
 * <div>
 * <h4>位图</h4>
 * <p>
 * 可以放png,jpg,gif,但是建议 <code>PNG</code>
 * 编译后的资源数据类型为BitmapDrawable
 * </P>
 * <p>
 * XML 位图是在 XML 中定义的资源，指向位图文件。实际上是原始位图文件的别名。XML 可以指定位图的其他属性，例如抖动和层叠。
 * </p>
 * <code>bitmap</code>是根标签,定义位图来源及其属性
 * <p>
 * 属性:
 * <ol>
 * <li>android:src 可绘制对象资源。必备。引用可绘制对象资源</li>
 * <li>android:antialias
 * 布尔值。启用或停用抗锯齿。</li>
 * <li>android:dither
 * 布尔值。当位图的像素配置与屏幕不同时（例如：ARGB 8888 位图和 RGB 565 屏幕），启用或停用位图抖动。</li>
 * <li>android:filter
 * 布尔值。启用或停用位图过滤。当位图收缩或拉伸以使其外观平滑时使用过滤</li>
 * <li>android:gravity
 * 关键字。定义位图的重力。重力指示当位图小于容器时，可绘制对象在其容器中放置的位置。</li>
 * <li>android:mipMap
 * 布尔值。启用或停用 mipmap 提示</li>
 * <li>android:tileMode
 * 关键字。定义平铺模式。当平铺模式启用时，位图会重复。重力在平铺模式启用时将被忽略。</li>
 * <li></li>
 * </ol>
 * </div>
 *
 * <div>
 * 转换可绘制对象：不支持超过2个项目(item).要向前转换，请调用 startTransition()。要向后转换，则调用 reverseTransition()。
 * <br>
 * item里面的top,left,bottom,right都是偏移
 * </div>
 *
 * <h3>裁剪可绘制对象 clipDrawable</h3>
 * <div>
 * xml标签 <code>clip</code>
 * <p>属性：<br>
 * <ol>
 * <li>android:drawable,可绘制对象资源。必备。引用要裁剪的可绘制对象资源。</li>
 * <li>android:clipOrientation,裁剪方向,horizontal水平裁剪，vertical垂直裁剪</li>
 * <li>android:gravity指定可绘制对象中要裁剪的位置</li>
 * </ol>
 * </p>
 * </div>
 * <h3>形状可绘制对象 GradientDrawable</h3>
 * <div>
 * <ol>
 * <li>shape 形状可绘制对象。这必须是根元素<br>
 * <code>android:shape</code>标签有4个可选值， <code>rectangle</code>矩形,默认。<br>
 * <code>oval</code>适应包含视图尺寸的椭圆形状。<br>
 * <code>line</code>跨越包含视图宽度的水平线。此形状需要 <stroke> 元素定义线宽。<br>
 * <code>ring</code>环形。环形还有5个可用属性。<br>
 * <code>android:innerRadius</code>尺寸。环内部（中间的孔）的半径<br>
 * <code>android:innerRadiusRatio</code>浮点型。环内部的半径，以环宽度的比率表示。例如，如果 android:innerRadiusRatio="5"，则内半径等于环宽度除以 5。此值被 android:innerRadius 覆盖。默认值为 9。<br>
 * <code>android:thickness</code>尺寸。环的厚度，<br>
 * <code>android:thicknessRatio</code>浮点型。环的厚度，表示为环宽度的比率。例如，如果 android:thicknessRatio="2"，则厚度等于环宽度除以 2。此值被 android:innerRadius 覆盖。默认值为 3。<br>
 * <code>android:useLevel</code>布尔值。如果这用作 LevelListDrawable，则此值为“true”。这通常应为“false”，否则形状不会显示。
 * </li>
 * </ol>
 * </div>
 */
public class DrawableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawable);

//        Drawable drawable = getResources().getDrawable(R.drawable.icon_anim_plane_1);
//        BitmapDrawable
        displayLevelListDrawable();
        displayTransitionDrawable();
        displayClipDrawable();
        displayScaleDrawable();
        displayGradientDrawable();
    }

    /**
     * shape,形状可绘制对象 GradientDrawable
     */
    private void displayGradientDrawable() {
        TextView shapeView=findViewById(R.id.tv_resources_shape);

//        shapeView.setOnClickListener(v->{
//            GradientDrawable drawable=new GradientDrawable();
//            ShapeDrawable
//        });
    }

    /**
     * 缩放可绘制对象，ScaleDrawable
     */
    private void displayScaleDrawable() {
        ImageView scaleView = findViewById(R.id.iv_resources_scale);
        ScaleDrawable drawable = (ScaleDrawable) scaleView.getDrawable();
        scaleView.setOnClickListener(v -> {
            drawable.setLevel(0);
            ValueAnimator valueAnimator = ValueAnimator.ofInt(0, 10000);
            valueAnimator.setDuration(1000);
            valueAnimator.addUpdateListener(animation -> drawable.setLevel((Integer) animation.getAnimatedValue()));
            valueAnimator.start();
        });
    }

    /**
     * 裁剪可绘制对象，ClipDrawable
     * <p>
     * 默认级别为 0，即完全裁剪，使图像不可见。当级别为 10,000 时，图像不会裁剪，而是完全可见。
     */
    private void displayClipDrawable() {
        ImageView clipView = findViewById(R.id.iv_resources_clip);
        ClipDrawable drawable = (ClipDrawable) clipView.getDrawable();

        clipView.setOnClickListener(v -> drawable.setLevel(drawable.getLevel() + 500));
    }

    boolean isReverse = false;

    /**
     * 转换可绘制对象-transitionDrawable
     */
    private void displayTransitionDrawable() {
        ImageView transition = findViewById(R.id.iv_resources_transition);
        TransitionDrawable drawable = (TransitionDrawable) transition.getDrawable();

        transition.setOnClickListener(v -> {
            if (!isReverse) {
                drawable.startTransition(500);
                isReverse = true;
            } else {
                drawable.reverseTransition(500);
                isReverse = false;
            }
        });
    }

    int level = 1;

    /**
     * 级别列表-levelListDrawable
     */
    private void displayLevelListDrawable() {
        ImageView levelImg = findViewById(R.id.iv_resources_level);
        levelImg.setOnClickListener(v -> {
            if (level > 7) {
                level = 1;
            }
            levelImg.setImageLevel(level);
            level++;
        });
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, DrawableActivity.class);
        context.startActivity(starter);
    }
}

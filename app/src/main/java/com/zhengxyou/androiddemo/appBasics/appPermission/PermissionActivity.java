package com.zhengxyou.androiddemo.appBasics.appPermission;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.zhengxyou.androiddemo.R;

/**
 * <a href="https://blog.csdn.net/u014088294/article/details/51924223">参考</a>
 * 权限请求一般有4步:
 * <ol>
 * <li>1、检查是否有这权限 {@link ActivityCompat#checkSelfPermission(Context, String)}</li>
 * <li>2、判断用户之前是否拒绝过了权限请求 {@link ActivityCompat#shouldShowRequestPermissionRationale(Activity, String)}</li>
 * <li>3、是的话，给出为什么需要这个权限，并请求权限，没拒绝过的话直接请求权限 {@link ActivityCompat#requestPermissions(Activity, String[], int)}</li>
 * <li>4、在权限请求结果回调方法里面做相关处理 {@link Activity#onRequestPermissionsResult(int, String[], int[])}</li>
 *
 * </ol>
 * <h4>自定义权限</h4>
 * <code>permission</code>标签
 * <p>
 * 对permission标签的各个属性大概说明一下，<br>
 * 1. name，该标签就是权限的名字啦。<br>
 * 2. description，该标签就是权限的介绍。<br>
 * 3. permissionGroup，指定该权限的组。<br>
 * 4. protectionLevel，指定保护级别。normal, dangerous, signature等。normal就是正常权限，该权限并不会给用户或者设备的隐私带来风险；dangerous就是危险权限，该级别的权限通常会给用户的数据或设备的隐私带来风险；signature指的是，只有相同签名的应用才能使用该权限<br>
 * </p>
 *
 * <a href="https://www.cnblogs.com/GnagWang/archive/2011/03/21/1990507.html">权限参考</a>
 * 检查某个 uid 和 pid (一般传本应用的，即检查本应用)是否有 permission 权限 {@link androidx.core.content.ContextCompat#checkSelfPermission(Context, String)}
 * <br>checkSelfPermission调用的也是checkPermission(String permission, int pid, int uid) 方法，<br>
 * <br>public int checkCallingPermission(String permission) // 检查调用者是否有 permission 权限，如果调用者是自己那么返回 PackageManager.PERMISSION_DENIED
 *
 * <br>public int checkCallingOrSelfPermission(String permission) // 检查自己或者其它调用者是否有 permission 权限
 * <p>
 * checkCallingOrSelfPermission和checkCallingPermission方法一般用于 <code>exported</code>为 <code>true</code>暴露给其他应用或者进程的情况下调用
 */
public class PermissionActivity extends AppCompatActivity {
    private TextView mPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);

        mPermission = findViewById(R.id.tv_permission_result);

        checkFeature();
        toCheckPermission();
        //跨进程IPC权限检查，checkCalling开头的基本都是跨进程
//        checkCallingOrSelfPermission()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(getPackageName() + ".permission.DEADLY_ACTIVITY") == PackageManager.PERMISSION_GRANTED) {
                mPermission.append("\n有自定义权限");
            } else {
                mPermission.append("\n无被授予自定义权限");
            }
        }
    }

    /**
     * 如果应用之前请求过此权限但用户拒绝了请求，{@link ActivityCompat#shouldShowRequestPermissionRationale(Activity, String)} 方法将返回 true
     * <p>
     * 如果用户在过去拒绝了权限请求，并在权限请求系统对话框中选择了 Don't ask again 选项，此方法将返回 false。如果设备规范禁止应用具有该权限，此方法也会返回 false。
     */
    private void toCheckPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            ContextCompat.checkSelfPermission(PermissionActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
            int permission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permission == PackageManager.PERMISSION_GRANTED) {
                mPermission.append("\n有存储权限");
            } else {
                mPermission.append("\n无存储权限");
//shouldShowRequestPermissionRationale()
                //如果应用之前请求过此权限但用户拒绝了请求，此方法将返回 true。拒绝请求可以提示用户为啥要这个权限
                if (ActivityCompat.shouldShowRequestPermissionRationale(PermissionActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ) {
                    mPermission.append("\n 已经拒绝过了额");
                    //再次请求
                    ActivityCompat.requestPermissions(PermissionActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 111);
                } else {
                    ActivityCompat.requestPermissions(PermissionActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 111);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 111 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mPermission.append("\n用户授予了存储权限");
        } else {
            mPermission.append("\n用户拒绝授予存储权限");
        }
    }

    /**
     * 检测缩放支持某个硬件
     */
    private void checkFeature() {
        boolean b = getPackageManager().hasSystemFeature("android.hardware.Camera2");
        mPermission.append("\n是否有相机硬件：" + b);
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, PermissionActivity.class);
        context.startActivity(starter);
    }
}

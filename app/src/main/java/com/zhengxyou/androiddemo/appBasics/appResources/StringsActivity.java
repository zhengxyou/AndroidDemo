package com.zhengxyou.androiddemo.appBasics.appResources;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.zhengxyou.androiddemo.R;

import java.util.Arrays;

/**
 * 转义撇号和引号<br>
 * 如果字符串中包含撇号 (')，您必须用反斜杠 (\') 将其转义，或为字符串加上双引号 ("")
 * <br>
 * 如果字符串中包含双引号，您必须将其转义（使用 \"）。 为字符串加上单引号不起作用。
 *
 * <a href="https://juejin.im/post/5b927af16fb9a05d2b6d9918">span参考，详细</a>
 * <a href="https://www.jianshu.com/p/bec5a54471c6">span参考</a>
 * <br>
 * Spanable中的常用常量：<br>
 * Spanned.SPAN_EXCLUSIVE_EXCLUSIVE --- 不包含start和end所在的端点                 (a,b)<br>
 * Spanned.SPAN_EXCLUSIVE_INCLUSIVE --- 不包含端start，但包含end所在的端点       (a,b]<br>
 * Spanned.SPAN_INCLUSIVE_EXCLUSIVE --- 包含start，但不包含end所在的端点          [a,b)<br>
 * Spanned.SPAN_INCLUSIVE_INCLUSIVE--- 包含start和end所在的端点<br>
 * what
 * 各种Span，不同的Span对应不同的样式，具体有如下:
 * <p>
 * 输入的字符才会起效果，或者说会改变的字符，只是写死的TextView里面的效果基本一直，要看具体效果可以用EditText试试
 * <p>
 * ForegroundColorSpan : 设置文本前景色（文本颜色）
 * BackgroundColorSpan : 设置背景颜色
 * AbsoluteSizeSpan : 设置绝对的文字大小，px单位
 * ClickableSpan : 为文字添加点击事件（类似于微信朋友圈评论列表中用户的昵称点击事件就可以用这个实现）
 * DynamicDrawableSpan :
 * ImageSpan : 文文本添加图片
 * RelativeSizeSpan : 设置相对文字大小，为倍数，相对于其他文字的大小
 * StrikethroughSpan : 添加删除线
 * SubscriptSpan : 设置下标文字
 * SuperscriptSpan : 设置上标文字
 * URLSpan : 文字设定超链接
 * UnderlineSpan : 设置下划线
 * <p>
 * SpannableString和SpannableStringBuilder的关系类似于String和StringBuilder。前者不可变，后者可变。所以两者的使用方法基本相同。
 */
public class StringsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_strings);

        showArrayString();
        showQuantityStrings();
        showFormat();
        showHtmlString();
        showSpannable();
    }

    private void showSpannable() {
        TextView mSpannable = findViewById(R.id.tv_resources_spannable);
//        Spannable
//        SpannableString
        SpannableStringBuilder ssb = new SpannableStringBuilder("public六static void main \n www.jianshu.com");
        ssb.setSpan(new BackgroundColorSpan(0x88ff0000), 0, 6, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ssb.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), 0, 7, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        ssb.setSpan(new URLSpan("www.jianshu.com"), 26, 41, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        ssb.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Toast.makeText(StringsActivity.this, String.valueOf(widget.getId()), Toast.LENGTH_SHORT).show();
            }
        }, 7, 13, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        //设置了点击事件后请加上这句，不然点击事件不起作用
        mSpannable.setMovementMethod(LinkMovementMethod.getInstance());

        Drawable drawable = getResources().getDrawable(R.drawable.ic_launcher);
        drawable.setBounds(0, 0, mSpannable.getLineHeight(), mSpannable.getLineHeight());
        ssb.setSpan(new ImageSpan(drawable, DynamicDrawableSpan.ALIGN_BASELINE), 14, 19, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mSpannable.setText(ssb);
    }

    private void showHtmlString() {
        TextView mMessageByHtml = findViewById(R.id.tv_resources_welcomeByHtml);
        mMessageByHtml.setText(Html.fromHtml(getResources().getString(R.string.welcome_messageByHtml)));

        TextView mMessageByHtmlColorSize = findViewById(R.id.tv_resources_welcomeByHtmlColorSize);
        mMessageByHtmlColorSize.setText(Html.fromHtml(getResources().getString(R.string.welcome_messageByHtmlColorSize)));

        TextView mMessageByHtmlBold = findViewById(R.id.tv_resources_welcomeBold);
        mMessageByHtmlBold.setText(Html.fromHtml(getResources().getString(R.string.welcome_message, "zhengxyou", 99)));
    }

    private void showFormat() {
        TextView mFormatView = findViewById(R.id.tv_resources_welcome);
        mFormatView.setText(getResources().getString(R.string.welcome_message, "zhengxyou", 99));
    }

    /**
     * 中文情况下并没有复数语法区分，但必须注意的是，某些语言（如中文）根本不做这些语法区分，因此您获取的始终是 other 字符串。
     * 所以需要设置成其他语言才有效果
     */
    @SuppressLint("SetTextI18n")
    private void showQuantityStrings() {
        TextView mQuantity = findViewById(R.id.tv_resources_quantity);
        String noSong = getResources().getQuantityString(R.plurals.resources_plurals_songs, 0);
        String oneSong = getResources().getQuantityString(R.plurals.resources_plurals_songs, 1, 1);
        String moreSongs = getResources().getQuantityString(R.plurals.resources_plurals_songs, 10, 10);

        mQuantity.setText(noSong + "\n" + oneSong + "\n" + "\n" + moreSongs);
    }

    private void showArrayString() {
        TextView mArray = findViewById(R.id.tv_resources_array);

        String[] stringArray = getResources().getStringArray(R.array.resources_array);

        mArray.setText(Arrays.toString(stringArray));
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, StringsActivity.class);
        context.startActivity(starter);
    }
}

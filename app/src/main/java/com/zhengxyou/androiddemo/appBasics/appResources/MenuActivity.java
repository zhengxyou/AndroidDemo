package com.zhengxyou.androiddemo.appBasics.appResources;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zhengxyou.androiddemo.R;

/**
 * <a href="https://developer.android.google.cn/guide/topics/resources/menu-resource?hl=zh-cn">menu</a>
 * <a href="https://blog.csdn.net/sl1990129/article/details/80815698">参考</a>
 * <p>
 * fragment中需要在 {@link androidx.fragment.app.Fragment#onCreate(Bundle)}方法中调用 {@link androidx.fragment.app.Fragment#setHasOptionsMenu(boolean)}
 * </p>
 * 可以通过 {@link AppCompatActivity#invalidateOptionsMenu()}刷新菜单Menu
 */
public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        registerForContextMenu(findViewById(R.id.tv_resources_contextMenu));

        displayPopupMenu();

        displayActionMode();
    }

    private void displayActionMode() {

        final ActionMode.Callback callback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                getMenuInflater().inflate(R.menu.resources_context_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                Toast.makeText(MenuActivity.this, item.getTitle(), Toast.LENGTH_SHORT).show();
                mode.finish();
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        };

        findViewById(R.id.Btn_resources_actionMode).setOnClickListener(v -> startActionMode(callback));
    }

    private void displayPopupMenu() {
        findViewById(R.id.Btn_resources_popupMenu).setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(this, v);
            getMenuInflater().inflate(R.menu.resources_popup_menu, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                Toast.makeText(MenuActivity.this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
            });
            popupMenu.show();
        });
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, MenuActivity.class);
        context.startActivity(starter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.resources_context_menu, menu);
    }

    @Override
    public void onContextMenuClosed(Menu menu) {
        super.onContextMenuClosed(menu);
        Toast.makeText(this, "onContextMenuClosed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.resources_contextMenu_add:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
            case R.id.resources_contextMenu_delete:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
            case R.id.resources_contextMenu_save:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    /**
     * 运行时修改Menu菜单没人
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.group2_item2).setVisible(true);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.resours_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.group3_item1) {
            item.setChecked(!item.isChecked());
            invalidateOptionsMenu();
            return true;
        }
//        switch (item.getItemId()){
//            case
//        }
//        invalidateOptionsMenu();
        return super.onOptionsItemSelected(item);
    }

    /**
     * 优先级大于onOptionsItemSelected
     */
    public void onGroupItemClick(MenuItem item) {
        Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
    }
}

package com.zhengxyou.androiddemo.appBasics.appResources.colorState;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.zhengxyou.androiddemo.R;

/**
 * <a href="https://developer.android.google.cn/guide/topics/resources/color-list-resource">Color state list</a>
 * ColorStateList颜色状态集合
 */
public class ColorStateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_state);
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, ColorStateActivity.class);
        context.startActivity(starter);
    }
}

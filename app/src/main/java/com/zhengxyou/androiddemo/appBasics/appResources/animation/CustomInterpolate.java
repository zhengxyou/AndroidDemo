package com.zhengxyou.androiddemo.appBasics.appResources.animation;

import android.view.animation.Interpolator;

/**
 * 自定义插值器，实现插值器Interpolator接口
 */
public class CustomInterpolate implements Interpolator {
    @Override
    public float getInterpolation(float input) {
        return 1 - input;
    }
}

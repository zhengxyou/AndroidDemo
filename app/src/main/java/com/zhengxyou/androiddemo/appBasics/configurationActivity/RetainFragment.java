package com.zhengxyou.androiddemo.appBasics.configurationActivity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * <pre>
 * 如果重启的Activity需要恢复大量的数据、重建网络或执行其他密集的操作，可以通过保留 <code>Fragment</code>来减轻重新初始化 <code>Activity</code>的负担
 * </pre>
 * <p>当 Android 系统因配置变更而关闭 Activity 时，不会销毁您已标记为要保留的 Activity 的片段。 您可以将此类片段添加到 Activity 以保留有状态的对象。</p>
 * <br>
 * <p>
 *     尽管可以存储任何对象，但是切勿传递与 Activity 绑定的对象，例如，Drawable、Adapter、View 或其他任何与 Context 关联的对象。否则，它将泄漏原始 Activity 实例的所有视图和资源。
 * </p>
 */
public class RetainFragment extends Fragment {
    private Object data;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //需要设置保留实例
        setRetainInstance(true);
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

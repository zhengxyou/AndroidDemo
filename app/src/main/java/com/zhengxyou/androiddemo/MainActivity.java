package com.zhengxyou.androiddemo;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.zhengxyou.androiddemo.activity.LifecycleActivity;
import com.zhengxyou.androiddemo.appBasics.appPermission.PermissionActivity;
import com.zhengxyou.androiddemo.appBasics.appResources.ResourcesActivity;

public class MainActivity extends AppCompatActivity {
    private static final String KEY_TAG = "TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        testNullExceptionBreakPoint();
        int j = getValue();
        Log.d(KEY_TAG, "onCreate: " + j);

        Log.d(KEY_TAG, "onCreate: " + j);

        for (int i = 0; i < 100; i++) {
            if (i == 55) {
                Log.d(KEY_TAG, "onCreate: " + i);
            }
        }

        initViews();
    }

    private void initViews() {
        findViewById(R.id.btn_main_appResources).setOnClickListener(v -> ResourcesActivity.start(MainActivity.this));
        findViewById(R.id.btn_main_permission).setOnClickListener(v -> PermissionActivity.start(MainActivity.this));
        findViewById(R.id.btn_main_activity).setOnClickListener(v -> LifecycleActivity.start(MainActivity.this));
    }

    private void testNullExceptionBreakPoint() {
        TextView tv = null;
        tv.setText("s");
    }

    public int getValue() {
        return 10;
    }
}
